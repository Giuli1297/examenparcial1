import java.io.*;
import java.net.*;
import java.util.*;

public class TCPServer{
    ArrayList vehiculosList; // Lista de Vehiculos a ser agregados
    PrintWriter writer; // Encargo de escribir mensajes al cliente
    BufferedReader reader; //Encargado de leer mensajes del cliente
    String message; //GUarda el mensaje del cliente

    public void go() {
        try {
            vehiculosList = new ArrayList();
            ServerSocket serverSock = new ServerSocket(4242); //Se establece el servidor
            while (true)
            {
                Socket sock = serverSock.accept(); //Se espera por la coneccion del cliente

                InputStreamReader isReader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(isReader); //Se establece el lector

                writer = new PrintWriter(sock.getOutputStream()); //Se establece el escritor a cliente
                while((message = reader.readLine()) != null){ //Se leen los mensajes del cliente aqui y se procesan de acuerdo al codigo que venga en el mensaje
                    if(Character.compare(message.charAt(0), '1') == 0){ //Un 1 al principio del mensaje representa agregacion de datos
                        vehiculosList.add(message);
                    }else if(Character.compare(message.charAt(0), '2') == 0){ //UN dos representa consulta de datos
                        String cedula = message.split("/")[1];
                        Iterator it = vehiculosList.iterator();
                        while(it.hasNext()){
                            String[] vehiculo = ((String) it.next()).split("/");
                            if(vehiculo[1].equals(cedula)){
                                writer.println("Cedula: "+vehiculo[1]+"\nNombre: "+vehiculo[2]+"\nApellido: "+vehiculo[3]+"\nChapa: "+vehiculo[4]+"\nMarca: "+vehiculo[5]);
                                writer.flush();
                            }
                        }
                    }
                }

                reader.close();
                writer.close();
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void main(String [] args){
        new TCPServer().go();
    }
}
