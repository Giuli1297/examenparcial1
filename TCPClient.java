import java.io.*;
import java.net.*;
import java.util.*;

public class TCPClient{
    Socket s; //Socket que establece la coneccion con el ServerS
    InputStreamReader streamReader;
    BufferedReader reader; //Para leer lo que provenga de server
    PrintWriter writer; //Para enviar mensajes al server
    BufferedReader scanner; //Para leer desde la linea de comandos

    public void go() {
        try {
            s = new Socket("127.0.0.1", 4242);
            streamReader = new InputStreamReader(s.getInputStream());
            reader = new BufferedReader(streamReader);
            writer = new PrintWriter(s.getOutputStream());
            scanner = new BufferedReader(new InputStreamReader(System.in));
            Thread treader = new Thread(new IncomingReader()); //Se crea un hilo que escuchar los mensajes del servidor en este caso las respuestas a consultas
            treader.start();

            System.out.println("***********************************");
            System.out.println("Si quieres agregar datos sobre un vehiculo ingresa 1\nSi quieres preguntar sobre un vehiculo ingresa 2\nSalir 3");
            System.out.println("***********************************");
            String choice = scanner.readLine();
            while(choice.equals("1") || choice.equals("2")){

                if(choice.equals("1")){
                    String vehiculo = "1/";
                    System.out.println("Cedula: ");
                    vehiculo = vehiculo + scanner.readLine() + "/";
                    System.out.println("Nombre: ");
                    vehiculo = vehiculo + scanner.readLine() + "/";
                    System.out.println("Apellido: ");
                    vehiculo = vehiculo + scanner.readLine() + "/";
                    System.out.println("Chapa: ");
                    vehiculo = vehiculo + scanner.readLine() + "/";
                    System.out.println("Marca: ");
                    vehiculo = vehiculo + scanner.readLine() + "/"; //Aqui se forma la estructura de string a ser enviada al servidor.
                    writer.println(vehiculo); //Se envia los datos a guardar al servidor
                    writer.flush();
                }else{
                    String question = "2/";
                    System.out.println("Cedula a consultar: ");
                    question = question + scanner.readLine() + "/";
                    writer.println(question); //Se envia una consulta al servidor
                    writer.flush();

                }
                Thread.sleep(1000); // Para que la respuesta de la consulta y el mensaje inferior no se interpongan
                System.out.println("***********************************");
                System.out.println("Si quieres agregar datos sobre un vehiculo ingresa 1\nSi quieres preguntar sobre un vehiculo ingresa 2\nSalir 3");
                System.out.println("***********************************");
                choice = scanner.readLine();
            }


            writer.close();
            reader.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    class IncomingReader implements Runnable {
        public void run() {
            String message;
            try {
                while ((message = reader.readLine()) != null) {
                    System.out.println(message);
                }
            } catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args)
    {
        new TCPClient().go();
    }
}
